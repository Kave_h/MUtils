﻿using System;

namespace MUtils.Configuration
{
    public interface IConfigurationService<out T> where T : BaseConfigurationModel
    {
        void Init<T>(string configName);
        T Get();
    }
}
