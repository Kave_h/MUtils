﻿using System;
using System.Collections.Generic;
using System.IO;
using MUtils.Configuration;
using MUtils.Interface;

namespace MUtils.JsonConfigurationProvider
{
    public class ConfigurationService<T> : IConfigurationService<T> where T : BaseConfigurationModel
    {
        private static bool DebugMode = false;
        private static string FileName => "Config";

        private static string DebugFileName => "Config-debug.json";

        private readonly ISerializer _serializer;
        //private readonly ICacher _memoryCache;
        //private readonly int _configCacheIntervalInSeconds;
        //private const string CacheKey = "configKey";

        private static Dictionary<Type,string> _configNames = new Dictionary<Type, string>();
        private static Dictionary<Type, string> _configs = new Dictionary<Type, string>();

        public ConfigurationService(ISerializer serializer)
        {
            _serializer = serializer;
            //_memoryCache = memoryCache;
            //if (!int.TryParse(ConfigurationManager.AppSettings["ConfigCacheIntervalInSeconds"],
            //    out _configCacheIntervalInSeconds))
            //    _configCacheIntervalInSeconds = 300;
        }

        public void SetDebugMode()
        {
            DebugMode = true;
        }

        public void Init<T1>(string configName)
        {
            if (!_configNames.ContainsKey(typeof(T)))
                _configNames.Add(typeof(T), configName);
        }

        public T Get()
        {
            if (_configs.TryGetValue(typeof(T), out var cnf))
            {
                return _serializer.Deserialize<T>(cnf);
            }

            _configNames.TryGetValue(typeof(T), out var fName);
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data", (!string.IsNullOrEmpty(fName) ? fName : FileName) + ".json");
            GetDebugConfigFileName(ref filePath,fName);
            var fileContent = File.ReadAllText(filePath);
            var configModel = _serializer.Deserialize<T>(fileContent);
            _configs.Add(typeof(T), fileContent);
            return configModel;
        }

        //[Conditional("DEBUG")]
        private void GetDebugConfigFileName(ref string path,string overrideFileName)
        {
            var debugMode = Environment.GetEnvironmentVariable("debugMode");
            if (!string.IsNullOrEmpty(debugMode) && debugMode == "true")
            {
                var debugfilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data", !string.IsNullOrEmpty(overrideFileName) ? overrideFileName + "-debug.json" : DebugFileName);
                if (File.Exists(debugfilePath))
                    path = debugfilePath;
            }
        }
    }
}