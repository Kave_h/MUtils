﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUtils.ConsoleLogRelay.Test
{
    class Program
    {
        private static readonly MemoryMappedFile Mmf = MemoryMappedFile.CreateNew("DbSyncer1", 10000000);
        private static int _counter = 0;
        static void Main(string[] args)
        {
            var ss = new string[]{"Dangeeeeeeer^red","Success^green","Warniiiiing^yello"};
            long counter = 0;
            while (true)
            {
                //var s = Console.ReadLine();
                var idx = Math.DivRem(counter, 3, out var rem);
                var s = " " + ss[rem];
                string sp = new string(' ', 100000 - s.Length);
                s += sp;
                var b = System.Text.Encoding.UTF8.GetBytes(s);
                using (MemoryMappedViewAccessor str = Mmf.CreateViewAccessor())
                {
                    str.WriteArray(0, b, 0, b.Length);
                }

                counter++;
                System.Threading.Thread.Sleep(5);

            }
        }
    }
}
