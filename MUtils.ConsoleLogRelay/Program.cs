﻿using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;

namespace MUtils.ConsoleLogRelay
{
    internal class Program
    {

        private static string background = "black";
        private static string foreground = "gray";
        private static void Main(string[] args)
        {
            string mmfName = "";
            string source = "";
            bool showHelp = false;
            int interval = 1;
            string filePath = "";
            bool colorFormat = false;
            string delimiters = "^";
            MemoryMappedFile mmf;
            void A(string result)
            {
                if (!string.IsNullOrEmpty(result))
                {
                    string[] lines = result.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    string[] d = delimiters.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string line in lines)
                    {
                        result = line;
                        if (colorFormat)
                        {

                            string[] splited = result.Split(d, StringSplitOptions.RemoveEmptyEntries);
                            string l = splited[0];
                            string c = splited.Length > 1 ? splited[1] : null;
                            Console.ForegroundColor = GetColor(c);
                            Console.WriteLine(l);
                        }
                        else
                        {
                            Console.WriteLine(result);
                        }
                    }
                }
            }
            if (args.Any())
            {
                try
                {

                    List<string> extras = ParseArgs(args, ref source, ref mmfName, ref filePath, ref showHelp, ref interval, ref colorFormat, ref background, ref foreground, ref delimiters);
                    Console.BackgroundColor = GetColor(background);
                    Console.Clear();
                    if (showHelp)
                    {
                        ShowHelp();
                        return;
                    }

                    switch (source)
                    {
                        case "mmf":
                            {
                                if (!string.IsNullOrEmpty(mmfName))
                                {
                                    Console.Title = "output log for " + mmfName;
                                    mmf = MemoryMappedFile.OpenExisting(mmfName, MemoryMappedFileRights.ReadWrite);
                                    var lastTime = DateTime.Now;
                                    var renewIntervaInSeconds = 30000;
                                    while (true)
                                    {
                                        string result = ReadMappedFileStream(mmf);
                                        A(result);
                                        System.Threading.Thread.Sleep(interval);
                                        if (DateTime.Now.Subtract(lastTime).TotalSeconds > renewIntervaInSeconds)
                                        {
                                            mmf = MemoryMappedFile.OpenExisting(mmfName, MemoryMappedFileRights.ReadWrite);
                                            lastTime = DateTime.Now;
                                        }
                                    }
                                }
                            }
                            break;
                        case "f":
                        case "file":
                            {
                                Console.Title = "output log for " + Path.GetFileName(filePath);
                                while (true)
                                {
                                    string result = ReadTail(filePath);
                                    A(result);
                                    System.Threading.Thread.Sleep(interval);
                                }
                            }
                    }

                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.ToString());
                    Console.ReadLine();
                }
            }
        }

        private static string ReadMappedFileStream(MemoryMappedFile mmf)
        {
            string result = "";
            using (MemoryMappedViewStream stream = mmf.CreateViewStream(0, 10000000))
            {
                BinaryReader reader = new BinaryReader(stream);
                string t = "";
                while (true)
                {
                    string y = reader.ReadString();
                    y = y.TrimStart().TrimEnd();
                    if (!string.IsNullOrEmpty(y))
                    {
                        t += y;
                    }
                    else
                    {
                        break;
                    }

                }
                if (!string.IsNullOrEmpty(t))
                {
                    result = t;
                }
            }
            using (MemoryMappedViewAccessor str = mmf.CreateViewAccessor())
            {
                string s = new string(' ', 100000);
                byte[] b = System.Text.Encoding.UTF8.GetBytes(s);
                str.WriteArray(0, b, 0, b.Length);
                _lastPos = b.Length;
            }

            return result;
        }

        private static void ShowHelp()
        {
            Console.WriteLine(@"#-------------HELP-------------#
-s  -source       Use Between these values : mmf (Memory Mapped File) ,f (File) ,e (Event Log : User Construction) 
-m  -memory       if you select Memory Mapped File as source log , then specify the name
-p  -path         If you select File as source log , then give the path to file
-cf -colorformat  With Color Formatting
-d  -delimiters   Delimiter(s) for color extraction ( use , to separate delimiters ) - Default delimiter is ^
-i  -interval     Set Check Interval in milliseconds
-bg -background   Set default background color
-fg -foreground   Set default foreground color
-h  -help         Show Help Page

Available Colors :   black ,cyan, darkblue, darkcyan, darkgray, darkgreen, darkmagenta, darkred, darkyellow, gray, green, blue, magenta, red, white, yellow
#------------------------------#");
        }

        private static long _lastPos = 0;

        private static string ReadTail(string filename)
        {
            using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (_lastPos == fs.Length)
                {
                    return null;
                }

                if (fs.Length < _lastPos)
                {
                    _lastPos = 0;
                }
                // Seek 1024 bytes from the end of the file
                fs.Seek(_lastPos, SeekOrigin.Begin);
                // read 1024 bytes
                byte[] bytes = new byte[fs.Length - _lastPos];
                fs.Read(bytes, 0, (int)(fs.Length - _lastPos));
                // Convert bytes to string
                string s = Encoding.Default.GetString(bytes);
                _lastPos = fs.Length;
                // or string s = Encoding.UTF8.GetString(bytes);
                // and output to console
                return s;
            }
        }

        private static List<string> ParseArgs(string[] args, ref string source, ref string mmfName, ref string filePath, ref bool showHelp, ref int interval,
            ref bool colorFormat, ref string background, ref string foreground, ref string delimiters)
        {
            string filePathTemp = null;
            int intervalTemp = 1;
            string mmfTemp = null;
            bool colorFormatTemp = false;
            bool showHelpTemp = false;
            string backgroundTemp = null;
            string foregroundTemp = null;
            string sourceTemp = null;
            string delimitersTemp = null;
            OptionSet os = new OptionSet()
            {
                {
                    "s|source=", "Source Log Type", value => { sourceTemp= value; }
                },
                {
                    "m|memory=", "MemoryMappedFile Name", value =>
                    {
                        mmfTemp = value;
                    }
                },
                {
                    "p|path=", "File Path", value =>
                    {
                        filePathTemp = value;
                    }
                },
                {
                    "i|interval=", "read interval", value => { intervalTemp = int.Parse(value); }
                },
                {
                    "cf|colorformat", "Color formatting", value => { colorFormatTemp= true; }
                },
                {
                    "bg|background=", "background color", value => { backgroundTemp = value; }
                },
                {
                    "fg|foreground=", "foreground color", value => { foregroundTemp = value; }
                },
                {
                    "d|delimiters=", "delimiters", value => { delimitersTemp= value; }
                },
                {
                    "h|help", "show help", value => { showHelpTemp = true; }
                }
            };
            List<string> extras = os.Parse(args);
            background = !string.IsNullOrEmpty(backgroundTemp) ? backgroundTemp : background;
            foreground = !string.IsNullOrEmpty(foregroundTemp) ? foregroundTemp : foreground;
            colorFormat = colorFormatTemp;
            delimiters = delimitersTemp ?? "^";
            mmfName = mmfTemp;
            interval = intervalTemp;
            filePath = filePathTemp;
            showHelp = showHelpTemp;
            source = sourceTemp;
            return extras;
        }

        private static ConsoleColor GetColor(string color)
        {
            if (string.IsNullOrEmpty(color))
            {
                return GetColor(foreground);
            }

            switch (color.ToLower())
            {
                case "black": return ConsoleColor.Black;
                case "cyan": return ConsoleColor.Cyan;
                case "darkblue": return ConsoleColor.DarkBlue;
                case "darkcyan": return ConsoleColor.DarkCyan;
                case "darkgray": return ConsoleColor.DarkGray;
                case "darkgreen": return ConsoleColor.DarkGreen;
                case "darkmagenta": return ConsoleColor.DarkMagenta;
                case "darkred": return ConsoleColor.DarkRed;
                case "darkyellow": return ConsoleColor.DarkYellow;
                case "gray": return ConsoleColor.Gray;
                case "green": return ConsoleColor.Green;
                case "blue": return ConsoleColor.Blue;
                case "magenta": return ConsoleColor.Magenta;
                case "red": return ConsoleColor.Red;
                case "white": return ConsoleColor.White;
                case "yellow": return ConsoleColor.Yellow;
                default: return GetColor(foreground);
            }
        }
    }
}
