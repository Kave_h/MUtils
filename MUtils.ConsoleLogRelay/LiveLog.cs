﻿using System.IO.MemoryMappedFiles;
using System.Threading;

namespace MUtils.ConsoleLogRelay
{
    public class LiveLog
    {
        private static MemoryMappedFile _mmf;
        private static int length = 0;
        private static readonly object Lock = new object();

        public LiveLog(string name,int sizeInBytes)
        {
            if (_mmf == null)
            {
                _mmf = MemoryMappedFile.CreateNew(name, sizeInBytes);
                length = sizeInBytes;
            }
        }

        public void Add(string s)
        {
            lock (Lock)
            {
                using (MemoryMappedViewAccessor str = _mmf.CreateViewAccessor())
                {
                    s = " " + s;
                    string sp = new string(' ', length - s.Length);
                    s += sp;
                    var b = System.Text.Encoding.UTF8.GetBytes(s);
                    str.WriteArray(0, b, 0, b.Length);
                }
            }
        }
    }
}