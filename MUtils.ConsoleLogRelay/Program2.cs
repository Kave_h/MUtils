﻿using NDesk.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace MUtils.ConsoleLogRelay
{
    internal class Program2
    {
        private static MemoryMappedFile Mmf = MemoryMappedFile.OpenExisting("Test1");

        private static void Main(string[] args)
        {

            while (true)
            {
                using (MemoryMappedViewStream stream = Mmf.CreateViewStream())
                {
                    BinaryReader reader = new BinaryReader(stream);
                    var s = reader.ReadString();
                    s = s.TrimStart().TrimEnd();
                    if (!string.IsNullOrEmpty(s))
                        Console.WriteLine(s);
                }
                using (MemoryMappedViewAccessor str = Mmf.CreateViewAccessor())
                {
                    var s = new string(' ', 100000);
                    var b = System.Text.Encoding.UTF8.GetBytes(s);
                    str.WriteArray(0, b, 0, b.Length);
                    lastPos = b.Length;
                }
                System.Threading.Thread.Sleep(1);
            }
        }

        private static long lastPos = 0;

        private static void ReadMappedFileStream()
        {
            using (MemoryMappedViewStream stream = Mmf.CreateViewStream())
            {
                BinaryReader reader = new BinaryReader(stream);
                var s = reader.ReadString();
                s = s.TrimStart().TrimEnd();
                if (!string.IsNullOrEmpty(s))
                    Console.WriteLine(s);
            }
            using (MemoryMappedViewAccessor str = Mmf.CreateViewAccessor())
            {
                var s = new string(' ', 100000);
                var b = System.Text.Encoding.UTF8.GetBytes(s);
                str.WriteArray(0, b, 0, b.Length);
                lastPos = b.Length;
            }
        }
    }
}
