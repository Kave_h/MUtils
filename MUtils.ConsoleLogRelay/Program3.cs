﻿using NDesk.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace MUtils.ConsoleLogRelay
{
    internal class Program
    {
        

        private static void Main(string[] args)
        {
            MemoryMappedFile Mmf;
            if (args.Any())
            {
                try
                {
                    string filePath = "";
                    bool showHelp = false;
                    int interval = 1;
                    bool noFormat = true;
                    bool colorFormat = false;
                    List<string> extras = ParseArgs(args, ref filePath, ref showHelp, ref interval, ref noFormat,
                        ref colorFormat);
                    if (showHelp)
                    {
                        ShowHelp();
                        return;
                    }

                    if (!string.IsNullOrEmpty(filePath))
                    {
                        Mmf = MemoryMappedFile.OpenExisting(mmfName);
                        while (true)
                        {
                            if (!string.IsNullOrEmpty(result))
                            {
                                string[] lines = result.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
                                foreach (var line in lines)
                                {
                                    result = line;
                                    if (noFormat)
                                    {
                                        Console.WriteLine(result);
                                    }
                                    else
                                    {
                                        if (colorFormat)
                                        {
                                            if (result.StartsWith("<"))
                                            {

                                            }
                                            else if (result.StartsWith("{"))
                                            {
                                                Regex regex = new Regex(@"\{.*?\}");
                                                MatchCollection matches = regex.Matches(result);
                                                foreach (Match match in matches)
                                                {
                                                    ColoredFormat model =
                                                        JsonConvert.DeserializeObject<ColoredFormat>(match.Value);
                                                    var c = model.color ?? model.c;
                                                    var l = model.log ?? model.l;
                                                    Console.ForegroundColor = GetColor(model.color ?? model.c);
                                                    Console.WriteLine(model.log ?? model.l);
                                                }

                                            }
                                            else
                                            {
                                                var splited = result.Split(new[] {"[:."},
                                                    StringSplitOptions.RemoveEmptyEntries);
                                                var l = splited[0];
                                                var c = splited.Length > 1 ? splited[1] : null;
                                                Console.ForegroundColor = GetColor(c);
                                                Console.WriteLine(l);
                                            }
                                        }
                                    }
                                }

                            }

                            System.Threading.Thread.Sleep(interval);
                        }
                    }
                }
                catch (Exception e)
                {
                    var i = 0;
                }

            }
        }

        private static long lastPos = 0;

        

        private static void ShowHelp()
        {
            Console.WriteLine(@"#-------------HELP-------------#
-f -file            Path to a single log file
-i    -interval     Set Check Interval in milliseconds
-nf  -noformat      With No Format -- this is the default type
-c   -color         With Color Formatting - log needs to be in this format : <color>red</color><log>messageM/log> or <c>red</c><l>message</l> or {""c"":""red"",""l"":""message""}
-h -help            Show Help Page
#------------------------------#");
        }

        private static List<string> ParseArgs(string[] args, ref string filePath, ref bool showHelp, ref int interval,
            ref bool noFormat, ref bool colorFormat)
        {
            int intervalTemp = 1;
            string filePathTemp = "";
            bool noFormatTemp = true;
            bool colorFormatTemp = false;
            bool showHelpTemp = false;
            OptionSet os = new OptionSet()
            {
                {
                    "f|file=", "file path", value =>
                    {
                        if (!File.Exists(value))
                        {
                            throw new Exception("file not found");
                        }

                        filePathTemp = value;
                    }
                },
                {
                    "i|interval=", "read interval", value => { intervalTemp = int.Parse(value); }
                },
                {
                    "nf|noformat", "read interval", value => { noFormatTemp = true; }
                },
                {
                    "c|color", "read interval", value => { colorFormatTemp = true; }
                },
                {
                    "h|help", "show help", value => { showHelpTemp = true; }
                }
            };
            List<string> extras = os.Parse(args);
            noFormat = colorFormatTemp == true ? false : noFormatTemp;
            colorFormat = colorFormatTemp;
            filePath = filePathTemp;
            interval = intervalTemp;
            showHelp = showHelpTemp;
            return extras;
        }

        private static ConsoleColor GetColor(string color)
        {
            if (string.IsNullOrEmpty(color)) return ConsoleColor.Gray;
            switch (color.ToLower())
            {
                case "black": return ConsoleColor.Black;
                case "cyan": return ConsoleColor.Cyan;
                case "darkblue": return ConsoleColor.DarkBlue;
                case "darkcyan": return ConsoleColor.DarkCyan;
                case "darkgray": return ConsoleColor.DarkGray;
                case "darkgreen": return ConsoleColor.DarkGreen;
                case "darkmagenta": return ConsoleColor.DarkMagenta;
                case "darkred": return ConsoleColor.DarkRed;
                case "darkyellow": return ConsoleColor.DarkYellow;
                case "gray": return ConsoleColor.Gray;
                case "green": return ConsoleColor.Green;
                case "blue": return ConsoleColor.Blue;
                case "magenta": return ConsoleColor.Magenta;
                case "red": return ConsoleColor.Red;
                case "white": return ConsoleColor.White;
                case "yellow": return ConsoleColor.Yellow;
                default: return ConsoleColor.Gray;
            }
        }

        private static void ReadMappedFileStream()
        {
            using (MemoryMappedViewStream stream = Mmf.CreateViewStream())
            {
                BinaryReader reader = new BinaryReader(stream);
                Console.WriteLine("Process A says: {0}", reader.ReadString());
            }
        }
    }

    public class ColoredFormat
    {
        public string c { get; set; }
        public string l { get; set; }
        public string color { get; set; }
        public string log { get; set; }
    }
}
