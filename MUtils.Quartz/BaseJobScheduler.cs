﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using Quartz.Impl.Matchers;

namespace MUtils.Quartz
{
    public class BaseJobScheduler<T> where T : IJob
    {
        public IScheduler Scheduler { get; private set; }
        private readonly IEnumerable<T> _jobs;

        public BaseJobScheduler(IEnumerable<T> jobs)
        {
            _jobs = jobs;
            Scheduler = new StdSchedulerFactory().GetScheduler().Result;
        }

        public void Start(Dictionary<string, Configuration> configs)
        {
            Scheduler.Start();
            foreach (T job in _jobs)
            {
                IJobDetail jobDetail = JobBuilder.Create(job.GetType()).Build();
                string name = jobDetail.JobType.Name;
                if (!configs.ContainsKey(name))
                {
                    continue;
                }

                if (configs[name].JobInstanceWithConfigAndName != null && configs[name].JobInstanceWithConfigAndName.Any())
                {
                    var cnf = GetScheduler3(configs[name]);
                    foreach (KeyValuePair<Action<SimpleScheduleBuilder>, (string uniqueId,Configuration cnf, Dictionary<string, object> dic)> item in cnf)
                    {
                        jobDetail = JobBuilder.Create(job.GetType())
                            .UsingJobData(new JobDataMap((IDictionary<string, object>)item.Value.dic))
                            .WithIdentity(item.Value.uniqueId)
                            .Build();

                        ITrigger trigger = TriggerBuilder.Create()
                            .WithSimpleSchedule(item.Key)
                            .WithIdentity(item.Value.uniqueId)
                            .StartAt(item.Value.cnf.StartAt.HasValue && item.Value.cnf.StartAt.Value.ToUniversalTime() > DateTime.UtcNow
                                ? item.Value.cnf.StartAt.Value.ToUniversalTime()
                                : item.Value.cnf.StartDelayInSeconds.HasValue
                                    ? DateTimeOffset.UtcNow.AddSeconds(item.Value.cnf.StartDelayInSeconds.Value)
                                    : DateTimeOffset.UtcNow)
                            .Build();

                        Scheduler.ScheduleJob(jobDetail, trigger);
                    }

                }
                else if (configs[name].JobInstanceWithConfig != null && configs[name].JobInstanceWithConfig.Any())
                {
                    IEnumerable<KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration cnf, Dictionary<string, object> dic)>> cnf = GetScheduler2(configs[name]);
                    foreach (KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration cnf, Dictionary<string, object> dic)> item in cnf)
                    {
                        jobDetail = JobBuilder.Create(job.GetType())
                            .UsingJobData(new JobDataMap((IDictionary<string, object>)item.Value.dic))
                            .WithIdentity(Guid.NewGuid().ToString())
                            .Build();

                        ITrigger trigger = TriggerBuilder.Create()
                            .WithSimpleSchedule(item.Key)
                            .StartAt(item.Value.cnf.StartAt.HasValue && item.Value.cnf.StartAt.Value.ToUniversalTime() > DateTime.UtcNow
                                ? item.Value.cnf.StartAt.Value.ToUniversalTime()
                                : item.Value.cnf.StartDelayInSeconds.HasValue
                                    ? DateTimeOffset.UtcNow.AddSeconds(item.Value.cnf.StartDelayInSeconds.Value)
                                    : DateTimeOffset.UtcNow)
                            .Build();

                        Scheduler.ScheduleJob(jobDetail, trigger);
                    }
                }
                else
                {
                    (Action<SimpleScheduleBuilder> builder, IEnumerable<Dictionary<string, object>> configs) sting = GetScheduler(configs[name]);
                    int startDelay = configs[name].StartDelayInSeconds ?? 0;

                    if (sting.builder == null)
                    {
                        continue;
                    }

                    if (sting.configs != null && sting.configs.Any())
                    {
                        foreach (Dictionary<string, object> item in sting.configs)
                        {
                            jobDetail = JobBuilder.Create(job.GetType())
                                .UsingJobData(new JobDataMap((IDictionary<string, object>)item))
                                .WithIdentity(Guid.NewGuid().ToString())
                                .Build();
                            ITrigger trigger = TriggerBuilder.Create()
                                .WithSimpleSchedule(sting.builder)
                                .StartAt(startDelay > 0 ? DateTimeOffset.UtcNow.AddSeconds(startDelay) : DateTimeOffset.UtcNow)
                                .Build();

                            Scheduler.ScheduleJob(jobDetail, trigger);
                        }
                    }
                    else
                    {
                        ITrigger trigger = TriggerBuilder.Create()
                            .WithSimpleSchedule(sting.builder)
                            .StartAt(startDelay > 0 ? DateTimeOffset.UtcNow.AddSeconds(startDelay) : DateTimeOffset.UtcNow)
                            .Build();
                        Scheduler.ScheduleJob(jobDetail, trigger);
                    }
                }
            }
        }
        
        public bool DisableTrigger(string jobUniqueName)
        {
            var allKeys = Scheduler.GetJobKeys(GroupMatcher<JobKey>.AnyGroup()).Result.ToList();
            if (!allKeys.Contains(JobKey.Create(jobUniqueName))) return false;
            Scheduler.DeleteJob(JobKey.Create(jobUniqueName));
            return true;
        }

        public bool AddJobToScheulder(string  jobUniqueId,Type jobType, IDictionary<string, object> data,int repeatCount,int intervalInSeconds)
        {
            var jobDetail = JobBuilder.Create(jobType)
                .UsingJobData(new JobDataMap(data))
                .WithIdentity(jobUniqueId)
                .Build();
            Action<SimpleScheduleBuilder> b = builder =>
            {
                if (repeatCount > 0)
                {
                    builder.WithRepeatCount(repeatCount);
                }
                else
                {
                    builder.RepeatForever();
                }
                if (intervalInSeconds>0)
                {
                    builder.WithInterval(TimeSpan.FromSeconds(intervalInSeconds));
                }

                builder.WithMisfireHandlingInstructionIgnoreMisfires();
            };
            ITrigger trigger = TriggerBuilder.Create()
                .WithSimpleSchedule(b)
                .Build();

            Scheduler.ScheduleJob(jobDetail, trigger);

            return true;
        }

        public void AddJobToScheduler(string jobName,string jobUniqueName, Configuration config)
        {
            T job = _jobs.FirstOrDefault(i => i.GetType().Name.Equals(jobName, StringComparison.CurrentCultureIgnoreCase));
            if (job == null)
            {
                throw new KeyNotFoundException($"no job with name {jobName} found in job directory");
            }

            IJobDetail jobDetail = JobBuilder.Create(job.GetType()).Build();
            string name = jobDetail.JobType.Name;
            (Action<SimpleScheduleBuilder> builder, IEnumerable<Dictionary<string, object>> configs) sting = GetScheduler(config);
            int startDelay = config.StartDelayInSeconds ?? 0;

            if (sting.builder == null)
            {
                throw new KeyNotFoundException("job builder not found");
            }

            if (sting.configs != null && sting.configs.Any())
            {
                foreach (Dictionary<string, object> item in sting.configs)
                {
                    jobDetail = JobBuilder.Create(job.GetType())
                        .UsingJobData(new JobDataMap((IDictionary<string, object>)item))
                        .WithIdentity(Guid.NewGuid().ToString())
                        .Build();
                    ITrigger trigger = TriggerBuilder.Create()
                        .WithSimpleSchedule(sting.builder)
                        .StartAt(startDelay > 0 ? DateTimeOffset.UtcNow.AddSeconds(startDelay) : DateTimeOffset.UtcNow)
                        .Build();

                    Scheduler.ScheduleJob(jobDetail, trigger);
                }
            }
            else
            {
                ITrigger trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(sting.builder)
                    .StartAt(startDelay > 0 ? DateTimeOffset.UtcNow.AddSeconds(startDelay) : DateTimeOffset.UtcNow)
                    .Build();
                Scheduler.ScheduleJob(jobDetail, trigger);
            }
        }
        public (Action<SimpleScheduleBuilder> builder, IEnumerable<Dictionary<string, object>> configs) GetScheduler(Configuration config)
        {
            bool enable = !config.Disable;
            if (!enable)
            {
                return (null, null);
            }

            int? interval = config.IntervalInSeconds;
            IEnumerable<string> types = config.Types;
            int? rCount = config.RepeatCount;
            Action<SimpleScheduleBuilder> b = builder =>
            {
                if (rCount.HasValue)
                {
                    builder.WithRepeatCount(rCount.Value);
                }
                else
                {
                    builder.RepeatForever();
                }
                if (interval.HasValue)
                {
                    builder.WithInterval(TimeSpan.FromSeconds(interval.Value));
                }

                builder.WithMisfireHandlingInstructionIgnoreMisfires();
            };

            List<Dictionary<string, object>> instances = new List<Dictionary<string, object>>();
            if (types != null)
            {
                foreach (string type in types)
                {
                    foreach (Dictionary<string, object> item in config.JobInstance)
                    {
                        item.Add("type", type);
                        instances.Add(item);
                    }
                }
            }

            return (b, instances.Any() ? instances : config.JobInstance);
        }

        public IEnumerable<KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration cnf, Dictionary<string, object> dic)>> GetScheduler2(Configuration config)
        {
            List<KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration cnf, Dictionary<string, object> dic)>> returnValue = new List<KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration cnf, Dictionary<string, object> dic)>>();
            if (config.Disable)
            {
                return returnValue;
            }

            foreach (KeyValuePair<Configuration, Dictionary<string, object>> cnf in config.JobInstanceWithConfig)
            {
                bool disable = cnf.Key.Disable;
                if (disable)
                {
                    continue;
                }

                int? interval = cnf.Key.IntervalInSeconds > 0 ? cnf.Key.IntervalInSeconds : config.IntervalInSeconds;
                int? rCount = cnf.Key.RepeatCount ?? config.RepeatCount;
                Action<SimpleScheduleBuilder> b = builder =>
                {
                    if (rCount.HasValue)
                    {
                        builder.WithRepeatCount(rCount.Value);
                    }
                    else
                    {
                        builder.RepeatForever();
                    }
                    if (interval.HasValue)
                    {
                        builder.WithInterval(TimeSpan.FromSeconds(interval.Value));
                    }

                    builder.WithMisfireHandlingInstructionIgnoreMisfires();
                };
                returnValue.Add(new KeyValuePair<Action<SimpleScheduleBuilder>, (Configuration, Dictionary<string, object>)>(b, (cnf.Key, cnf.Value)));
            }
            return returnValue;
        }
        public IEnumerable<KeyValuePair<Action<SimpleScheduleBuilder>, (string uniqueId, Configuration cnf, Dictionary<string, object> dic)>> GetScheduler3(Configuration config)
        {
            var returnValue = new List<KeyValuePair<Action<SimpleScheduleBuilder>, (string uniqueId, Configuration cnf, Dictionary<string, object> dic)>>();
            if (config.Disable)
            {
                return returnValue;
            }

            foreach (KeyValuePair<Configuration,(string jobUniqueId,Dictionary<string, object> ExtraData)> cnf in config.JobInstanceWithConfigAndName)
            {
                bool disable = cnf.Key.Disable;
                if (disable)
                {
                    continue;
                }

                int? interval = cnf.Key.IntervalInSeconds > 0 ? cnf.Key.IntervalInSeconds : config.IntervalInSeconds;
                int? rCount = cnf.Key.RepeatCount ?? config.RepeatCount;
                Action<SimpleScheduleBuilder> b = builder =>
                {
                    if (rCount.HasValue)
                    {
                        builder.WithRepeatCount(rCount.Value);
                    }
                    else
                    {
                        builder.RepeatForever();
                    }
                    if (interval.HasValue)
                    {
                        builder.WithInterval(TimeSpan.FromSeconds(interval.Value));
                    }

                    builder.WithMisfireHandlingInstructionIgnoreMisfires();
                };
                returnValue.Add(
                    new KeyValuePair<Action<SimpleScheduleBuilder>, (string uniqueId, Configuration cnf,
                        Dictionary<string, object> dic)>(b, (cnf.Value.jobUniqueId, cnf.Key, cnf.Value.ExtraData)));
            }
            return returnValue;
        }
        public void ReScheduleJob(Configuration config, TriggerKey triggerKey, DateTime? startAt)
        {
            (Action<SimpleScheduleBuilder> builder, IEnumerable<Dictionary<string, object>> configs) scheduler = GetScheduler(config);
            ITrigger trigger = null;
            if (startAt.HasValue)
            {
                trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(scheduler.builder)
                    .StartAt(startAt.Value)
                    .Build();
            }
            else
            {
                TriggerBuilder.Create()
                    .WithSimpleSchedule(scheduler.builder)
                    .StartNow()
                    .Build();
            }
            Scheduler.RescheduleJob(triggerKey, trigger);
        }
        public void StartJob(JobKey key)
        {

        }
    }
}
